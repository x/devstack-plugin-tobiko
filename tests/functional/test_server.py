# Copyright (c) 2019 Red Hat
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
from __future__ import absolute_import

import pytest

import tobiko
from tobiko.openstack import stacks
from tobiko.shell import ping
from tobiko.shell import sh


@pytest.fixture
def server_stack() -> stacks.ServerStackFixture:
    return tobiko.setup_fixture(stacks.CirrosServerStackFixture)


def test_ssh(server_stack: stacks.ServerStackFixture):
    """Test SSH connectivity to floating IP address"""
    hostname = sh.ssh_hostname(ssh_client=server_stack.ssh_client)
    assert server_stack.server_name.lower() == hostname


def test_ping(server_stack: stacks.ServerStackFixture):
    """Test ICMP connectivity to floating IP address"""
    ping.assert_reachable_hosts([server_stack.floating_ip_address])
