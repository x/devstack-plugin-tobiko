# Copyright (c) 2019 Red Hat
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
from __future__ import absolute_import

from tobiko.openstack import topology
from tobiko.shell import ping
from tobiko.shell import sh


def test_ssh():
    """Test SSH connectivity to devstack nodes"""
    for node in topology.list_openstack_nodes():
        assert node.name == sh.ssh_hostname(ssh_client=node.ssh_client)


def test_ping():
    """Test ICMP connectivity to devstack nodes"""
    ips = [node.public_ip
           for node in topology.list_openstack_nodes()]
    ping.assert_reachable_hosts(ips)
