# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

# Customize the count of CPU cores on the VM
CPUS = 4

# Customize the amount of memory on the VM
MEMORY = ENV.fetch("VM_SIZE", "8192").to_i

# Every Vagrant development environment requires a box. You can search for
# boxes at https://vagrantcloud.com/search.
BOX = ENV.fetch("VM_BOX", "generic/ubuntu2204")

# Machine host name
HOSTNAME = "devstack"

# Top vagrantfile dir
VAGRANTFILE_DIR = File.dirname(__FILE__)

# Source provision playbook
PROVISION_PLAYBOOK = ENV.fetch(
  "PROVISION_PLAYBOOK", "#{VAGRANTFILE_DIR}/playbooks/vagrant/provision.yaml")

# Playbook directory
PROVISION_DIR = File.dirname(PROVISION_PLAYBOOK)

# Host IP address to be assigned to OpenStack in DevStack
HOST_NETWORK = "192.168.56"
TENANT_NETWORK = "192.168.57"

# Local directory from where look for required projects files
PROJECTS_DIR = File.dirname(ENV.fetch('PROJECTS_DIR', VAGRANTFILE_DIR))

GIT_BASE = ENV.fetch('GIT_BASE', 'https://opendev.org')

TOX_ENVLIST = ENV.fetch('TOX_ENVLIST', '')
TOX_EXTRA_ARGS = ENV.fetch('TOX_EXTRA_ARGS', '--notest')

# Allow to switch configuration
DEVSTACK_CONF_NAME = ENV.fetch('DEVSTACK_CONF_NAME', 'ovn')

DEVSTACK_LOCAL_CONF_FILE = ENV.fetch(
  'DEVSTACK_LOCAL_CONF_FILE',
  "#{PROVISION_DIR}/#{DEVSTACK_CONF_NAME}/local.conf" )


# Multi-node configuration is still not working. There are issues with setting up
# networking to be handled.
NODES_COUNT = 1


# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = BOX
  # config.vm.box_version = "< 3.0"
  config.vm.hostname = HOSTNAME

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: HOST_IP

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #

  node_memory = [4096, (MEMORY / NODES_COUNT).to_i].max
  node_cpus = [4, (CPUS / NODES_COUNT).to_i].max
  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = false
    vb.cpus = node_cpus
    vb.memory = node_memory
  end

  config.vm.provider "libvirt" do |libvirt|
    libvirt.qemu_use_session = false
    libvirt.cpus = node_cpus
    libvirt.memory = node_memory
  end

  for node_id in 0..(NODES_COUNT - 2)
    secondary_hostname =  "#{HOSTNAME}-secondary-#{node_id}"
    config.vm.define secondary_hostname, primary: false do |secondary|
      secondary.vm.hostname = secondary_hostname
      secondary.vm.network "private_network", ip: "#{HOST_NETWORK}.#{20 + node_id}"
      secondary.vm.network "private_network", ip: "#{TENANT_NETWORK}.#{20 + node_id}"
    end
  end

  primary_hostname = "#{HOSTNAME}-primary"
  config.vm.define "devstack-primary", primary: true do |primary|
    primary.vm.hostname = primary_hostname
    primary.vm.network "private_network", ip: "#{HOST_NETWORK}.10"
    primary.vm.network "private_network", ip: "#{TENANT_NETWORK}.10"

    # Run provision playbook
    primary.vm.provision "ansible" do |ansible|
      ansible.limit = 'all'
      ansible.playbook = PROVISION_PLAYBOOK
      ansible.extra_vars = ansible.extra_vars = {
        'devstack_local_conf_file' => DEVSTACK_LOCAL_CONF_FILE,
        'tox_envlist' => TOX_ENVLIST,
        'tox_extra_args' => TOX_EXTRA_ARGS,
      }
    end
  end
end
