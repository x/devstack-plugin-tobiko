======================
Tobiko Devstack Plugin
======================


DevStack plugin to configure Tobiko framework on a DevStack provisioned host
----------------------------------------------------------------------------

The only goal of this plugin is generating a valid /etc/tobiko/tobiko.conf file
to be consumed by Tobiko Python framework on a machine that is running DevStack.

This translates DevStack configurations from `local.conf` file to tobiko's
configuration file. It is intended to facilitate running Tobiko test cases from
an host that has been provisioned using DevStack project.


References
----------

* Free software: Apache License, Version 2.0
* Source code: https://opendev.org/x/tobiko-devstack
* Bugs: https://storyboard.openstack.org/#!/project/x/tobiko


Related projects
~~~~~~~~~~~~~~~~
* Tobiko: https://tobiko.readthedocs.io/en/latest/
* OpenStack: https://www.openstack.org/
* DevStack: https://docs.openstack.org/devstack/latest/
